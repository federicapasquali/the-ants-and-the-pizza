import argparse
import numpy as np
import operator

class Shape:
    '''Base class for shapes defining the food distribution.'''
    # Center of the shape
    x0 = None
    y0 = None
    # Width and height of the shape
    width = None
    height = None

    # Function to find out if there is food at coordinate (x, y)
    def is_food(self, x, y):
        return not self.is_no_food(x, y)

    def is_no_food(self, x, y):
        '''Opposite of is_food.'''
        raise NotImplementedError()

class Ellipse(Shape):
    def __init__(self, x0, y0, half_width, half_height):
        '''Ellipse parametrized by its center point (x0, y0) and half
        its width (half_width) and half its height (half_width)
        according to the equation
        ((x-x0)/half_width)**2 + ((y-y0)/half_height)**2 = 1.
        '''
        self.x0 = x0
        self.y0 = y0
        self.width = 2*half_width
        self.height = 2*half_height
        self.is_no_food = lambda x, y: ((x-x0)/half_width)**2 + ((y-y0)/half_height)**2 < 1

class Rectangle(Shape):
    def __init__(self, x0, y0, x1, y1):
        '''Rectangle parametrized by its lower left (x0, x1) and
        upper right coordinates (x1, y1).
        '''
        # Attention: meaning of x0 and y0 changes.
        self.x0 = (x0 + x1) / 2
        self.y0 = (y0 + y1) / 2
        self.width = abs(x1 - x0)
        self.height = abs(y1 - y0)
        self.is_no_food = lambda x, y: (x0 < x and x < x1 and y0 < y and y < y1)


class Grid:
    def __init__(self, args):
        '''Initialize the grid with one shape. Inputs from argparse.'''
        # Build Shape class, adding more shapes is not going to ne a problem like this
        known_shapes = {"ellipse": Ellipse, "rectangle": Rectangle}
        self.shape = None
        for shape_name, class_ in known_shapes.items():
            if shape_name not in args or not vars(args)[shape_name]:
                continue
            if self.shape:
                print(f"Already created another shape. Only use one of the following command-line arguments {known_shapes.keys()}")
            self.shape = class_(*vars(args)[shape_name])
        if not self.shape:
            print(f"Please specify one of the following command-line arguments {known_shapes.keys()}")
            return

        self.time_step = args.time_step
        self.time = 0

        # Define functions for conversion between user coordinates
        # (coords) and grid indices (grid)
        min_x = self.shape.x0 - self.shape.width / 2
        max_x = self.shape.x0 + self.shape.width / 2
        min_y = self.shape.y0 - self.shape.height / 2
        max_y = self.shape.y0 + self.shape.height / 2

        step_size_x = args.step_size
        step_size_y = args.step_size

        steps_left =  int(np.ceil(abs(min_x / step_size_x)))
        steps_right = int(np.ceil(abs(max_x / step_size_x)))
        steps_down =  int(np.ceil(abs(min_y / step_size_y)))
        steps_up =    int(np.ceil(abs(max_y / step_size_y)))

        # size of grid in each dimension (+1 for origin)
        n_x = steps_left + steps_right + 1
        n_y = steps_down + steps_up + 1

        self.grid_prob = np.zeros(shape=(n_x, n_y))

        self.conv_grid_to_coords_x = lambda x_index: (x_index-steps_left) * step_size_x
        self.conv_grid_to_coords_y = lambda y_index: (y_index-steps_down) * step_size_y
        self.conv_grid_to_coords = lambda x_index, y_index: (self.conv_grid_to_coords_x(x_index), self.conv_grid_to_coords_y(y_index))

        self.conv_coords_to_grid_x = lambda x: int((x / step_size_x) + steps_left)
        self.conv_coords_to_grid_y = lambda y: int((y / step_size_y) + steps_down)
        self.conv_coords_to_grid = lambda x, y: (self.conv_coords_to_grid_x(x), self.conv_coords_to_grid_y(y))


    def set_probability(self, x, y, probability):
        '''Set the probability of being at point (x, y)'''
        x_index, y_index = self.conv_coords_to_grid(x, y)
        self.grid_prob[x_index, y_index] = probability

    def sum_tuples(a, b):
        '''Sum tuples entry-by-entry.'''
        return tuple(map(operator.add, a, b))

    def update(self, p_right=0.25, p_up=0.25, p_left=0.25, p_down=0.25):
        '''Update grid by letting probabilities evolve in each
        direction. Count time up. If the sum of probabilities of
        moving in the directions is smaller than 1, the remaining
        term determines the probability of remaining at the previous
        position.'''

        p_move = p_right + p_up + p_left + p_down
        if p_move > 1:
            print(f"Probabilities don't add up: {p_right}, {p_up}, {p_left}, {p_down}")
            return
        p_stay = 1 - p_move

        new_grid = np.zeros(shape=self.grid_prob.shape)
        it = np.nditer(self.grid_prob, flags=['multi_index'], order='F')
        for prob in it:
            # We stop the simulation once the ant has reached food.
            # This also takes care of the boundary conditions.
            if self.shape.is_food(*self.conv_grid_to_coords(*it.multi_index)):
                continue

            new_grid[Grid.sum_tuples(it.multi_index, (1, 0))] += prob * p_right
            new_grid[Grid.sum_tuples(it.multi_index, (0, 1))] += prob * p_up
            new_grid[Grid.sum_tuples(it.multi_index, (-1, 0))] += prob * p_left
            new_grid[Grid.sum_tuples(it.multi_index, (0, -1))] += prob * p_down
            new_grid[it.multi_index] += prob * p_stay
        self.time = self.time + self.time_step
        self.grid_prob = new_grid

    def get_prob_on_food(self):
        '''Loop over grid and sum all probabilities at positions with
        food.'''
        it = np.nditer(self.grid_prob, flags=['multi_index'], order='F')
        sum_prob = 0
        for prob in it:
            if self.shape.is_food(*self.conv_grid_to_coords(*it.multi_index)):
                sum_prob += prob
        return sum_prob

    def remove_ants_on_food(self):
        '''Set all probabilities to 0 at positions with food.'''
        it = np.nditer(self.grid_prob, flags=['multi_index'], op_flags=['readwrite'], order='F')
        for prob in it:
            if self.shape.is_food(*self.conv_grid_to_coords(*it.multi_index)):
                prob = 0
        it.close()
        return prob

    def __str__(self):
        return self.__repr__()


def main(args):
    '''Simulate ant movement on grid for a certain time.'''

    # Number of seconds to simulate
    n_seconds = 200

    # Get grid of correct size
    grid = Grid(args)

    # Place ant hill in origin
    grid.set_probability(0, 0, 1)

    # Estimate of the average time and the series of estimates at
    # every step in time
    avg_time = 0.0
    avg_time_evolution = [avg_time]
    while True:
        # Simulate one time unit on the grid
        grid.update()
        # Update average time depending on probability to reach food
        avg_time += grid.get_prob_on_food() * grid.time
        avg_time_evolution.append(avg_time)
        # Remove cases in which ants have reached food from simulation
        grid.remove_ants_on_food()
        if grid.time >= n_seconds:
            break

    print(f"The estimated average time for ants to reach food in the current configuration is {int(np.rint(avg_time))} seconds (estimated after simulating {n_seconds} time intervals with the final simulation value {avg_time}).")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--time_step", type=float, help="simulation speed in s", default=1)
    parser.add_argument("--speed", type=float, help="ant's speed in cm/s", default=10)
    parser.add_argument("--step_size", type=float, help="step size of ant in cm", default=10)
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("--ellipse", type=float, nargs=4, metavar=('X0', 'Y0', 'HALF_WIDTH', 'HALF_HEIGHT'), help="center x, center y, half width, half height")#, default=[2.5, 2.5, 30, 40])
    group.add_argument("--rectangle", type=float, nargs=4, metavar=('X0', 'Y0', 'X1', 'Y1'), help="bottom left and top right coordinates")

    args = parser.parse_args()
    main(args)
